#!/bin/bash

# verify user is root
if [ $USER != "root" ]; then
    echo "You must be root to run this script (sudo -E)" > /dev/fd/2
    exit 1
fi

if [ $(xdg-user-dir HOME) = "/root" ]; then
    echo "You must run this script with sudo -E to keep your environment"
    exit 1
fi

if [ $# -ne 1 ] || ( [ $1 != 'gather' ] && [ $1 != 'install' ] ); then
    echo "Usage: gather | install"
    exit 1
fi

if [ $1 == 'gather' ]; then

    ##############
    ### GATHER ###
    ##############

    # intial folders clean
    rm -rf dotfiles/*
    rm -rf scripts/*
    rm -rf diverse/*
    rm -rf pacman-hooks/*
    rm -rf systemd-units/*
    mkdir -p systemd-units/system
    mkdir -p systemd-units/user

    # config files
    cp $HOME/.config/nvim/init.vim dotfiles/nvimrc
    cp $HOME/.zshrc dotfiles/zshrc
    cp $HOME/.profile dotfiles/profile
    cp $HOME/.config/zathura/zathurarc dotfiles/zathurarc
    cp $HOME/.config/alacritty/alacritty.yml dotfiles/alacritty.yml
    cp $HOME/.config/redshift/redshift.conf dotfiles/redshift.conf
    cp $HOME/.config/gtk-3.0/settings.ini dotfiles/gtk-settings.ini
    cp $HOME/.config/Vieb/viebrc dotfiles/viebrc
    cp $HOME/.config/waybar/config dotfiles/waybar-config
    cp $HOME/.config/waybar/style.css dotfiles/waybar.css
    cp $HOME/.config/git/config dotfiles/git-config
    cp $HOME/.config/mimeapps.list dotfiles/mimeapps.list
    cp $HOME/.config/mako/config dotfiles/mako-config
    cp $HOME/.gnupg/gpg-agent.conf dotfiles/gpg-agent.conf

    # system config
    cp /etc/environment system-config/environment

    # sway config
    cp $HOME/.config/sway/config dotfiles/sway-config
    cp $HOME/.config/sway/env dotfiles/sway-env
    cp /usr/share/wayland-sessions/sway-session.desktop diverse/sway-session.desktop

    # sway scripts
    cp /usr/local/bin/sway-random-color scripts/sway-random-color
    cp /usr/local/bin/sway-change-sink-volume scripts/sway-change-sink-volume
    cp /usr/local/bin/sway-change-source-volume scripts/sway-change-source-volume
    cp /usr/local/bin/sway-mute-sink scripts/sway-mute-sink
    cp /usr/local/bin/sway-mute-source scripts/sway-mute-source
    cp /usr/local/bin/sway-service scripts/sway-service
    cp /usr/local/bin/sway-set-brightness scripts/sway-set-brightness
    cp /usr/local/bin/sway-random-wallpaper scripts/sway-random-wallpaper
    cp /usr/local/bin/sway-lock scripts/sway-lock

    # other scripts
    cp $HOME/.config/waybar/nordvpn.sh scripts/waybar-nordvpn.sh
    cp $HOME/.config/waybar/listening-ports.sh scripts/waybar-listening-ports.sh
    cp $HOME/.config/waybar/pacman.sh scripts/waybar-pacman.sh
    cp /usr/local/bin/update-materia-theme-gdm scripts/update-materia-theme-gdm
    cp /usr/local/bin/update-initramfs-mode scripts/update-initramfs-mode

    # pacman hooks
    cp /etc/pacman.d/hooks/waybar-pacman.hook pacman-hooks/waybar-pacman.hook
    cp /etc/pacman.d/hooks/update-materia-theme-gdm.hook pacman-hooks/update-materia-theme-gdm.hook
    cp /etc/pacman.d/hooks/firejail-update-symlinks.hook pacman-hooks/firejail-update-symlinks.hook
    cp /etc/pacman.d/hooks/update-initramfs-mode.hook pacman-hooks/update-initramfs-mode.hook

    # systemd general units
    cp /etc/systemd/user/sway.service systemd-units/user/sway.service
    cp /etc/systemd/user/sway-session.target systemd-units/user/sway-session.target
    cp /etc/systemd/user/sway-wallpaper.service systemd-units/user/sway-wallpaper.service
    cp /etc/systemd/user/sway-idle.service systemd-units/user/sway-idle.service
    cp /etc/systemd/user/mpris-proxy.service systemd-units/user/mpris-proxy.service

elif [ $1 == 'install' ]; then

    ###############
    ### INSTALL ###
    ###############

    # config files
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/nvimrc $HOME/.config/nvim/init.vim
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/zshrc $HOME/.zshrc
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/profile $HOME/.profile
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/zathurarc $HOME/.config/zathura/zathurarc
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/alacritty.yml $HOME/.config/alacritty/alacritty.yml
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/redshift.conf $HOME/.config/redshift/redshift.conf
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/gtk-settings.ini $HOME/.config/gtk-2.0/settings.ini
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/gtk-settings.ini $HOME/.config/gtk-3.0/settings.ini
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/gtk-settings.ini $HOME/.config/gtk-4.0/settings.ini
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/viebrc $HOME/.config/Vieb/viebrc
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/waybar-config $HOME/.config/waybar/config
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/waybar.css $HOME/.config/waybar/style.css
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/git-config $HOME/.config/git/config
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/mimeapps.list $HOME/.config/mimeapps.list
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/mako-config $HOME/.config/mako/config
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/gpg-agent.conf $HOME/.gnupg/gpg-agent.conf

    # system config
    install -Dm 644 -o root -g root system-config/environment /etc/environment

    # sway config
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/sway-config $HOME/.config/sway/config
    install -Dm 644 -o $USERNAME -g $USERNAME dotfiles/sway-env $HOME/.config/sway/env
    install -Dm 644 -o $USERNAME -g $USERNAME diverse/sway-session.desktop /usr/share/wayland-sessions/sway-session.desktop

    # sway scripts
    install -Dm 755 -o root -g root scripts/sway-random-color /usr/local/bin/sway-random-color
    install -Dm 755 -o root -g root scripts/sway-change-sink-volume /usr/local/bin/sway-change-sink-volume
    install -Dm 755 -o root -g root scripts/sway-change-source-volume /usr/local/bin/sway-change-source-volume
    install -Dm 755 -o root -g root scripts/sway-mute-sink /usr/local/bin/sway-mute-sink
    install -Dm 755 -o root -g root scripts/sway-mute-source /usr/local/bin/sway-mute-source
    install -Dm 755 -o root -g root scripts/sway-service /usr/local/bin/sway-service
    install -Dm 755 -o root -g root scripts/sway-set-brightness /usr/local/bin/sway-set-brightness
    install -Dm 755 -o root -g root scripts/sway-random-wallpaper /usr/local/bin/sway-random-wallpaper
    install -Dm 755 -o root -g root scripts/sway-lock /usr/local/bin/sway-lock

    # other scripts
    install -Dm 755 -o $USERNAME -g $USERNAME scripts/waybar-nordvpn.sh $HOME/.config/waybar/nordvpn.sh 
    install -Dm 755 -o $USERNAME -g $USERNAME scripts/waybar-listening-ports.sh $HOME/.config/waybar/listening-ports.sh 
    install -Dm 755 -o $USERNAME -g $USERNAME scripts/waybar-pacman.sh $HOME/.config/waybar/pacman.sh 
    install -Dm 755 -o root -g root scripts/update-materia-theme-gdm /usr/local/bin/update-materia-theme-gdm
    install -Dm 755 -o root -g root scripts/update-initramfs-mode /usr/local/bin/update-initramfs-mode

    # pacman hooks
    install -Dm 644 -o root -g root pacman-hooks/waybar-pacman.hook /etc/pacman.d/hooks/waybar-pacman.hook
    install -Dm 644 -o root -g root pacman-hooks/update-materia-theme-gdm.hook /etc/pacman.d/hooks/update-materia-theme-gdm.hook
    install -Dm 644 -o root -g root pacman-hooks/firejail-update-symlinks.hook /etc/pacman.d/hooks/firejail-update-symlinks.hook
    install -Dm 644 -o root -g root pacman-hooks/update-initramfs-mode.hook /etc/pacman.d/hooks/update-initramfs-mode.hook

    # systemd general units
    install -Dm 644 -o root -g root systemd-units/user/sway.service /etc/systemd/user/sway.service
    install -Dm 644 -o root -g root systemd-units/user/sway-session.target /etc/systemd/user/sway-session.target
    install -Dm 644 -o root -g root systemd-units/user/sway-idle.service /etc/systemd/user/sway-idle.service
    install -Dm 644 -o root -g root systemd-units/user/sway-wallpaper.service /etc/systemd/user/sway-wallpaper.service
    install -Dm 644 -o root -g root systemd-units/user/mpris-proxy.service /etc/systemd/user/mpris-proxy.service

    echo "To do:"
    echo "systemctl --user daemon-reload"
    echo "systemctl --user enable sway-idle.service sway-wallpaper.service"
fi

echo "Done."
