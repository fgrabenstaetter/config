#!/bin/bash

vpn_status=$(nordvpn status | grep "Status" | cut -d : -f 2 | cut -d " " -f 2)
vpn_country=$(nordvpn status | grep "Status" --after 1 | tail -n 1 | cut -d " " -f 3 | grep -Eo "^[a-z]{2}" | tr a-z A-Z)

if [ $vpn_status = "Connecting" ]; then
    class="connecting"
elif [ $vpn_status = "Connected" ]; then
    class="connected"
else
    class="disconnected"
fi

text="NordVPN"
if [ $class != "disconnected" ]; then
    text="$text [$vpn_country]"
fi

echo "{ "\""text"\"": "\""$text"\"", "\""class"\"": "\""$class"\"" }"
