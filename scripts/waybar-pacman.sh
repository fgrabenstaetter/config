#!/bin/bash

nb_updates=$(checkupdates | wc -l)
echo $nb_updates

if [ $nb_updates -ne 0 ]; then
    exit 0
else
    exit 1
fi
