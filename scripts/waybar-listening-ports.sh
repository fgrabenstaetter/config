#!/bin/bash

text=$(lsof -i -P | grep "LISTEN" | grep -v "localhost" | awk '{print $9 }' | cut -d ':' -f 2 | uniq | sed 's/\\n/ - /')
echo $text

if [ -n "$text" ]; then
    exit 0
else
    exit 1
fi
